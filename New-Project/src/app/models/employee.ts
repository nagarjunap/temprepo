export interface Employee {

	employeeId:number;
	firstName:string;
	lastName:string;
	age:number;
	role:string;
	isActive?:boolean;
}
