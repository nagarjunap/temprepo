import { Injectable, OnInit } from '@angular/core';
import { Employee } from '../models/employee';
import { Roles } from '../models/roles.enum';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService{

  employeesList:Employee[];

  constructor() {
    this.getEmployees();
  }

  addEmployee(employee:Employee):void{
    this.employeesList.push(employee);
  }

  private getEmployees(): void{
    this.employeesList = 
  	 [
  		{
  			employeeId:1,
  			firstName:'Sharath',
  			lastName:'Kumar',
  			age:25,
  			isActive:true,
  			role:Roles.Admin
  		},
  		{
  			employeeId:2,
  			firstName:'Sharath',
  			lastName:'Kumar',
  			age:27,
  			role:Roles.Developer
  		},
  		{
  			employeeId:3,
  			firstName:'Karthik',
  			lastName:'Reddy',
  			age:29,
  			role:Roles.Developer
  		},
  		{
  			employeeId:4,
  			firstName:'Sumesh',
  			lastName:'Babu',
  			age:24,
  			role:Roles.Developer
  		},
  		{
  			employeeId:5,
  			firstName:'Anil',
  			lastName:'Sahu',
  			age:30,
  			role:Roles.Admin
  		}
  	]
  }
}
