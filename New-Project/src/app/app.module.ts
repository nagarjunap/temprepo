import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AddEmployeeComponent } from './components/add-employee/add-employee.component';
import { AllEmployeesComponent } from './components/all-employees/all-employees.component';

const routes: Routes = [
  { path: '', component: AllEmployeesComponent },
  { path: 'allEmployees', component: AllEmployeesComponent },
  { path: 'allEmployees/addEmployee', component: AddEmployeeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    AddEmployeeComponent,
    AllEmployeesComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
