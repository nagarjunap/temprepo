import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../models/employee';
import { Roles } from '../../models/roles.enum';

@Component({
  selector: 'app-all-employees',
  templateUrl: './all-employees.component.html',
  styleUrls: ['./all-employees.component.css']
})
export class AllEmployeesComponent implements OnInit {

  employees:Employee[];
  isAdmins:boolean;
  isDevelopers:boolean;
  admins:Employee[];
  developers:Employee[];
  keyss:Array<string>;

  constructor(private service:EmployeeService){
  	
  }

  ngOnInit(){
  	this.employees = this.service.employeesList;
    console.log(this.employees);
  	// this.getDevelopers();
  	this.getAdmins();
  }

  getAdmins():void{
  	this.isDevelopers = false;
  	this.isAdmins = true;
  	this.admins = this.employees.filter(e => e.role == Roles.Admin);
  }
  getDevelopers():void{
  	this.isAdmins = false;
  	this.isDevelopers = true;
  	this.developers = this.employees.filter(e => e.role == Roles.Developer);
  }
  deleteEmployee(emp:Employee):void{
    console.log('>>>before '+this.service.employeesList.length);
    var i = this.service.employeesList.length;
    while(i--){
      if(emp.employeeId == this.service.employeesList[i].employeeId){
        this.service.employeesList.splice(i,1);
      }
    }
    
    console.log('>>> deleted emp: '+JSON.stringify(emp));
    console.log('>>>After '+this.service.employeesList.length);
  }
  getMeta(key:number):void{
    var keyss:Array<string> = Object.keys(this.employees[0]);
    this.keyss = keyss;
    // return keyss[key];
  }
}
