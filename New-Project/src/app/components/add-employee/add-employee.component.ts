import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormsModule, ReactiveFormsModule, FormBuilder, Validators  } from '@angular/forms';
import { Router } from '@angular/router';

import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../models/employee'

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
	addEmployee: FormGroup;
	/*addEmployee = new FormGroup({
		id: new FormControl(''),
		firstName: new FormControl(''),
		lastName: new FormControl(''),
		age: new FormControl(''),
		role: new FormControl('')
	});*/
	emp:Employee;
  constructor(private service:EmployeeService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
        this.addEmployee = this.formBuilder.group({
        	employeeId: ['', [Validators.required,Validators.pattern(/\-?\d*\.?\d{1,2}/)]],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            age: ['', Validators.required],
            role: ['', Validators.required]
        });
    }

  addEmployees():void{
  	 console.log(JSON.stringify(this.addEmployee.value));
  	 this.emp = JSON.parse(JSON.stringify(this.addEmployee.value)) as Employee;
	console.log(this.service.employeesList.length);
  	this.service.addEmployee(this.emp);
  	console.log(this.service.employeesList.length);
  	this.router.navigate(['allEmployees']);
  }
}
