import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../utilities/employee'

@Component({
  selector: 'add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

	addEmployee: FormGroup;

  constructor(private service:EmployeeService, private formBuilder: FormBuilder, private router:Router) {
   }

  ngOnInit() {
  	this.addEmployee = this.formBuilder.group({
  		employeeId: ['', Validators.required],
  		firstName: ['', Validators.required],
  		lastName: ['', Validators.required],
  		department: ['', Validators.required]
  	});
  }

  newEmployee(): void{
  	var emp = JSON.parse(JSON.stringify(this.addEmployee.value)) as Employee;
  	this.service.addEmployee(emp);
  	this.router.navigate(['']);
  }

  cancel(): void{
  	this.router.navigate(['']);
  }
}
