import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../utilities/employee';
import { Department } from '../../utilities/department.enum';

@Component({
  selector: 'all-employees',
  templateUrl: './all-employees.component.html',
  styleUrls: ['./all-employees.component.css']
})
export class AllEmployeesComponent implements OnInit {

	employees: Employee[];
	admins: Employee[];
	developers: Employee[];
	isAdmin:boolean = false;
	isDeveloper:boolean = false;
	meta:string[];

  constructor(private service: EmployeeService) { }

  ngOnInit() {
  	this.employees = this.service.employees;
  	if(this.employees.length>0){
  		this.meta = Object.keys(this.employees[0])
  	}
  	this.showAdmins();
  }

  delete(emp:Employee): void{
  	var index = this.service.employees.indexOf(emp);
  	this.service.employees.splice(index, 1);
  }

  showAdmins():void{
  	this.isDeveloper = false;
  	this.isAdmin = true;
  	this.admins = this.employees.filter(emp => emp.department == Department.Admin);
  }

  showDevelopers():void{
  	this.isAdmin = false;
  	this.isDeveloper = true;
  	this.developers = this.employees.filter(emp => emp.department == Department.Developer);
  }

}
