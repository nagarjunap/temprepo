export interface Employee {

	employeeId:number,
	firstName:string,
	lastName:string,
	department:string,
	isActive?:boolean
}
