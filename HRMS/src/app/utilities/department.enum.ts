export enum Department {

	Developer = 'Developer',
	Admin = 'Admin',
	HR = 'HR'
}
