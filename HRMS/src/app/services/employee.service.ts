import { Injectable, OnInit } from '@angular/core';
import { Employee } from '../utilities/employee';
import { Department } from '../utilities/department.enum'

@Injectable({
  providedIn: 'root'
})
export class EmployeeService{

  employees:Employee[];
  constructor() { this.getEmployees(); }

  getEmployees():void {

  	this.employees =  [
  		{
  			employeeId: 1,
  			firstName: 'Sharath',
  			lastName: 'Kumar',
  			department: Department.Developer,
  			isActive: true
  		},
  		{
  			employeeId: 2,
  			firstName: 'Karthik',
  			lastName: 'Reddy',
  			department: Department.HR,
  			isActive: true
  		},
  		{
  			employeeId: 3,
  			firstName: 'Surya',
  			lastName: 'Pratab',
  			department: Department.Developer,
  			isActive: true
  		},
  		{
  			employeeId: 4,
  			firstName: 'Kalyan',
  			lastName: 'Babu',
  			department: Department.Admin,
  			isActive: true
  		},
  	];
  }

  addEmployee(emp:Employee):void{
    this.employees.push(emp);
  }
}
